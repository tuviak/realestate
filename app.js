System.register(['@angular/core', "@angular/forms", '@angular/platform-browser', '@angular/platform-browser-dynamic', "ng2-completer"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, forms_1, platform_browser_1, platform_browser_dynamic_1, ng2_completer_1;
    var Screen, Result, SearchResult, PropertySelector, PropertyApp, SearchProperies;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (platform_browser_dynamic_1_1) {
                platform_browser_dynamic_1 = platform_browser_dynamic_1_1;
            },
            function (ng2_completer_1_1) {
                ng2_completer_1 = ng2_completer_1_1;
            }],
        execute: function() {
            /*Main Screen*/
            Screen = (function () {
                function Screen() {
                }
                return Screen;
            }());
            /*Data class for results*/
            Result = (function () {
                function Result(propertyType) {
                    this.propertyType = propertyType;
                }
                return Result;
            }());
            /*SearchResult class*/
            SearchResult = (function () {
                function SearchResult() {
                }
                SearchResult = __decorate([
                    core_1.Component({
                        selector: 'property-search-result',
                        inputs: ['result'],
                        host: { class: 'myRow' },
                        template: "\n\t\t<div class=\"margin-bottom\">\n\t\t\t<div class=\"container\" >\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"card-block col-md-6 table-bordered \">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t\t\t<h4 class=\"card-title\">{{result.address}}, {{result.suburb}},{{result.state}}</h4>\n\t\t\t\t\t\t\t\t<p class=\"card-text\"></p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t\t\t\t<img class=\"img-fluid center-block product-item\" src=\"images/{{result.images[0]}}.jpg\" alt=\"\">\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-6 col-md-4\">\n\t\t\t\t\t\t\t\t\t<img class=\"img-fluid center-block product-item\" src=\"images/{{result.images[1]}}.jpg\" alt=\"\">\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-6 col-md-4\">\n\t\t\t\t\t\t\t\t\t<img class=\"img-fluid center-block product-item\" src=\"images/{{result.images[2]}}.jpg\" alt=\"\">\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-6 col-md-4\">\n\t\t\t\t\t\t\t\t\t<img class=\"img-fluid center-block product-item\" src=\"images/{{result.images[3]}}.jpg\" alt=\"\">\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<a href=\"#\" class=\"btn btn-primary margin-bottom\">Details</a>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"card-block col-md-6\">\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t"
                    }), 
                    __metadata('design:paramtypes', [])
                ], SearchResult);
                return SearchResult;
            }());
            PropertySelector = (function () {
                function PropertySelector() {
                    this.myStates = ['New South Wales', 'Australian Capital Territory', 'Queensland', 'South Australia', 'Western Australia', 'Tasmania'];
                    this.results = [];
                }
                PropertySelector.prototype.addArticle = function (propertyType, minBeds, maxBeds, area) {
                    var searchResults = [
                        { "propertyType": "house", "beds": "3", "suburb": "Cherrybrook", "address": "56 County Dr", "State_short": "NSW", "State_long": "New South Wales", "Country_long": "Australia", "Country_short": "AUS", "coords": [-33.728846, 151.032389], "Title": "AUCTION 24th JUNE AT 11:30AM", "Agent": "Igor Guvic", "Details": "Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.", "images": ["1", "2", "3", "4", "5", "6"] },
                        { "propertyType": "house", "beds": "4", "suburb": "Cherrybrook", "address": "6 Dantic Pl", "State_short": "NSW", "State_long": "New South Wales", "Country_long": "Australia", "Country_short": "AUS", "coords": [-33.727493, 151.030418], "Title": "AUCTION 24th JUNE AT 11:30AM", "Agent": "Igor Guvic", "Details": "Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.", "images": ["1", "2", "3", "4", "5", "6"] },
                        { "propertyType": "app_unit", "beds": "4", "suburb": "Cherrybrook", "address": "12A Haven Ct", "State_short": "NSW", "State_long": "New South Wales", "Country_long": "Australia", "Country_short": "AUS", "coords": [-33.728526, 151.029384], "Title": "AUCTION 24th JUNE AT 11:30AM", "Agent": "Igor Guvic", "Details": "Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.", "images": ["1", "2", "3", "4", "5", "6"] },
                        { "propertyType": "app_unit", "beds": "4", "suburb": "Cherrybrook", "address": "47 County Dr", "State_short": "NSW", "State_long": "New South Wales", "Country_long": "Australia", "Country_short": "AUS", "coords": [-33.729630, 151.030639], "Title": "AUCTION 24th JUNE AT 11:30AM", "Agent": "Igor Guvic", "Details": "Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.", "images": ["1", "2", "3", "4", "5", "6"] },
                        { "propertyType": "app_unit", "beds": "5", "suburb": "Cherrybrook", "address": "66 John Rd", "State_short": "NSW", "State_long": "New South Wales", "Country_long": "Australia", "Country_short": "AUS", "coords": [-33.730674, 151.034115], "Title": "AUCTION 24th JUNE AT 11:30AM", "Agent": "Igor Guvic", "Details": "Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.", "images": ["1", "2", "3", "4", "5", "6"] },
                        { "propertyType": "house", "beds": "5", "suburb": "Cherrybrook", "address": "153 David Rd", "State_short": "NSW", "State_long": "New South Wales", "Country_long": "Australia", "Country_short": "AUS", "coords": [-33.727248, 151.029062], "Title": "AUCTION 24th JUNE AT 11:30AM", "Agent": "Igor Guvic", "Details": "Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.", "images": ["1", "2", "3", "4", "5", "6"] }
                    ];
                    for (var _i = 0, searchResults_1 = searchResults; _i < searchResults_1.length; _i++) {
                        var searchResult = searchResults_1[_i];
                        if ((searchResult.propertyType == propertyType.value || propertyType.value == 'All') && ((searchResult.beds >= minBeds.value && searchResult.beds <= maxBeds.value))) {
                            this.results.push(searchResult);
                            ;
                        }
                    }
                    return false;
                };
                PropertySelector = __decorate([
                    core_1.Component({
                        selector: 'property-search',
                        template: "\n\t<div class=\"page-header\">\n\t\t<h3 class=\"\">Search Property</h3>\n\t</div>\n\t<div class=\" justify-content-md-center\">\n\t\t<form class=\"form-group\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-6 col-md-2 form-control\">\n\t\t\t\t\t<label for=\"title\">Search for:</label>\n\t\t\t\t\t<select name=\"searchFor\" class=\"form-control\" #searchFor>\n\t\t\t\t\t  <option value=\"buy\">Buy</option>\n\t\t\t\t\t  <option value=\"rent\">Rent</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-6 col-md-2 form-control\">\n\t\t\t\t\t<label for=\"title\">Property Type:</label>\n\t\t\t\t\t<select name=\"propertyType\" class=\"form-control\" #propertyType>\n\t\t\t\t\t  <option value=\"All\">All</option>\n\t\t\t\t\t  <option value=\"house\">House</option>\n\t\t\t\t\t  <option value=\"townhouse\">Townhouse</option>\n\t\t\t\t\t  <option value=\"land\">Land</option>\n\t\t\t\t\t  <option value=\"app_unit\">Apartment & Unit</option>\n\t\t\t\t\t  <option value=\"villa\">Villa</option>\n\t\t\t\t\t  <option value=\"rural\">Rural</option>\n\t\t\t\t\t  <option value=\"acreage\">Acreage</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"col-6 col-md-2 form-control\">\n\t\t\t\t\t<label for=\"minBeds\">Min.Beds:</label>\n\t\t\t\t\t<select name=\"minBeds\" class=\"form-control\" #minBeds>\n\t\t\t\t\t  <option value=\"All\">All</option>\n\t\t\t\t\t  <option value=\"1\">1</option>\n\t\t\t\t\t  <option value=\"2\">2</option>\n\t\t\t\t\t  <option value=\"3\">3</option>\n\t\t\t\t\t  <option value=\"4\">4</option>\n\t\t\t\t\t  <option value=\"5\">5</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-6 col-md-2 form-control\">\n\t\t\t\t\t<label for=\"maxBeds\">Max.Beds:</label>\n\t\t\t\t\t<select name=\"maxBeds\" class=\"form-control\" #maxBeds>\n\t\t\t\t\t  <option value=\"All\">All</option>\n\t\t\t\t\t  <option value=\"1\">1</option>\n\t\t\t\t\t  <option value=\"2\">2</option>\n\t\t\t\t\t  <option value=\"3\">3</option>\n\t\t\t\t\t  <option value=\"4\">4</option>\n\t\t\t\t\t  <option value=\"5\">5</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-2 form-control\">\n\t\t\t\t\t<label for=\"title\">Min. Price:</label>\n\t\t\t\t\t<select name=\"propertyMinPrice\" class=\"form-control\" #propertyMinPrice>\n\t\t\t\t\t  <option value=\"Any\">Any</option>\n\t\t\t\t\t  <option value=\"1\">50,000</option>\n\t\t\t\t\t  <option value=\"2\">100,000</option>\n\t\t\t\t\t  <option value=\"3\">200,000</option>\n\t\t\t\t\t  <option value=\"4\">300,000</option>\n\t\t\t\t\t  <option value=\"5\">400,000</option>\n\t\t\t\t\t  <option value=\"6\">500,000</option>\n\t\t\t\t\t  <option value=\"7\">600,000</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div  class=\"col-md-2 form-control\">\n\t\t\t\t\t<label for=\"title\">Max. Price:</label>\n\t\t\t\t\t<select name=\"propertyMaxPrice\" class=\"form-control\" #propertyMaxPrice>\n\t\t\t\t\t  <option value=\"Any\">Any</option>\n\t\t\t\t\t  <option value=\"1\">50,000</option>\n\t\t\t\t\t  <option value=\"2\">100,000</option>\n\t\t\t\t\t  <option value=\"3\">200,000</option>\n\t\t\t\t\t  <option value=\"4\">300,000</option>\n\t\t\t\t\t  <option value=\"5\">400,000</option>\n\t\t\t\t\t  <option value=\"6\">500,000</option>\n\t\t\t\t\t  <option value=\"7\">600,000</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group row\">\n\t\t\t\t<div  class=\"col-md-3 form-control\">\n\t\t\t\t\t<label for=\"title\">State:</label>\n\t\t\t\t\t<select name=\"state\" class=\"form-control\" #state>\n\t\t\t\t\t  <option value=\"Any\">Any</option>\n\t\t\t\t\t  <option value=\"1\">New South Wales</option>\n\t\t\t\t\t  <option value=\"2\">ACT</option>\n\t\t\t\t\t  <option value=\"3\">Victoria</option>\n\t\t\t\t\t  <option value=\"4\">South Australia</option>\n\t\t\t\t\t  <option value=\"5\">West Australia</option>\n\t\t\t\t\t  <option value=\"6\">North Territory</option>\n\t\t\t\t\t  <option value=\"7\">Quensland</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div  class=\"col-md-3 form-control\">\n\t\t\t\t\t<label for=\"ares\">City:</label>\n\t\t\t\t\t<input name=\"city\" #city class=\"form-control\">\n\t\t\t\t</div>\t\n\t\t\t\t<div  class=\"col-md-3 form-control\">\n\t\t\t\t\t<label for=\"ares\">Area:</label>\n\t\t\t\t\t<ng2-completer type=input [(ngModel)]=\"myState\" [ngModelOptions]=\"{standalone: true}\" [datasource]=\"myStates\" [minSearchLength]=\"0\" class=\"form-control\" name=\"area\" #area></ng2-completer>\n\t\t\t\t</div>\n\n\t\t\t\t<div  class=\"col-md-3 form-control\">\n\t\t\t\t\t<label for=\"ares\">Key words:</label>\n\t\t\t\t\t<input name=\"word\" #word  class=\"form-control\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"form-group row\">\n\t      \t\t<div class=\"\">\n\t\t\t\t\t<button class=\"btn btn-primary\" (click)=\"addArticle(propertyType, minBeds,maxBeds,area)\">\n\t\t\t\t\t\tSearch\n\t\t\t\t\t</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t</form>\n\t</div>\n\t\t\n\n\t<div class=\"\">\n\t\t<property-search-result *ngFor=\"let result of results\" [result]=\"result\">\n\t\t</property-search-result>\t\n\t</div>\n  "
                    }), 
                    __metadata('design:paramtypes', [])
                ], PropertySelector);
                return PropertySelector;
            }());
            /*Main class*/
            PropertyApp = (function () {
                function PropertyApp() {
                }
                PropertyApp = __decorate([
                    core_1.Component({
                        selector: 'property-app',
                        template: "\n\t\t<div class=\"property-app\">\n\t\t\t<property-search></property-search>\n\t    </div>\n" }), 
                    __metadata('design:paramtypes', [])
                ], PropertyApp);
                return PropertyApp;
            }());
            SearchProperies = (function () {
                function SearchProperies() {
                }
                SearchProperies = __decorate([
                    core_1.NgModule({
                        declarations: [PropertyApp, PropertySelector, SearchResult],
                        imports: [platform_browser_1.BrowserModule, ng2_completer_1.Ng2CompleterModule, forms_1.FormsModule],
                        bootstrap: [PropertyApp]
                    }), 
                    __metadata('design:paramtypes', [])
                ], SearchProperies);
                return SearchProperies;
            }());
            platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(SearchProperies);
        }
    }
});
//# sourceMappingURL=app.js.map