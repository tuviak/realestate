import {
	NgModule,
	Component
} from '@angular/core';
import { FormsModule } from "@angular/forms";
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Ng2CompleterModule } from "ng2-completer";
import { CompleterService, CompleterData } from 'ng2-completer';


/*Main Screen*/
class Screen{
	constractor(
		public title:string{

		}
	)
}
/*Data class for results*/
class Result{
	propertyType: string;

	constructor(propertyType: string){
		this.propertyType=propertyType;
	}		
}

/*SearchResult class*/
@Component({
	selector: 'property-search-result',
	inputs: ['result'],
	host: {class: 'myRow'},
	template: `
		<div class="margin-bottom">
			<div class="container" >
				<div class="row">
					<div class="card-block col-md-6 table-bordered ">
						<div class="row">
							<div class="col-md-12">
								<h4 class="card-title">{{result.address}}, {{result.suburb}},{{result.state}}</h4>
								<p class="card-text"></p>
							</div>
							<div class="row">
								<div class="col-md-12">
									<img class="img-fluid center-block product-item" src="images/{{result.images[0]}}.jpg" alt="">
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<img class="img-fluid center-block product-item" src="images/{{result.images[1]}}.jpg" alt="">
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<img class="img-fluid center-block product-item" src="images/{{result.images[2]}}.jpg" alt="">
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<img class="img-fluid center-block product-item" src="images/{{result.images[3]}}.jpg" alt="">
								</div>
							</div>
						</div>
						<a href="#" class="btn btn-primary margin-bottom">Details</a>
					</div>
					<div class="card-block col-md-6">
					</div>
				</div>
			</div>
		</div>
	`
	})
class SearchResult{
}

@Component({
	selector: 'property-search',
template: `
	<div class="page-header">
		<h3 class="">Search Property</h3>
	</div>
	<div class=" justify-content-md-center">
		<form class="form-group">
			<div class="row">
				<div class="col-6 col-md-2 form-control">
					<label for="title">Search for:</label>
					<select name="searchFor" class="form-control" #searchFor>
					  <option value="buy">Buy</option>
					  <option value="rent">Rent</option>
					</select>
				</div>
				<div class="col-6 col-md-2 form-control">
					<label for="title">Property Type:</label>
					<select name="propertyType" class="form-control" #propertyType>
					  <option value="All">All</option>
					  <option value="house">House</option>
					  <option value="townhouse">Townhouse</option>
					  <option value="land">Land</option>
					  <option value="app_unit">Apartment & Unit</option>
					  <option value="villa">Villa</option>
					  <option value="rural">Rural</option>
					  <option value="acreage">Acreage</option>
					</select>
				</div>

				<div class="col-6 col-md-2 form-control">
					<label for="minBeds">Min.Beds:</label>
					<select name="minBeds" class="form-control" #minBeds>
					  <option value="All">All</option>
					  <option value="1">1</option>
					  <option value="2">2</option>
					  <option value="3">3</option>
					  <option value="4">4</option>
					  <option value="5">5</option>
					</select>
				</div>
				<div class="col-6 col-md-2 form-control">
					<label for="maxBeds">Max.Beds:</label>
					<select name="maxBeds" class="form-control" #maxBeds>
					  <option value="All">All</option>
					  <option value="1">1</option>
					  <option value="2">2</option>
					  <option value="3">3</option>
					  <option value="4">4</option>
					  <option value="5">5</option>
					</select>
				</div>
				<div class="col-md-2 form-control">
					<label for="title">Min. Price:</label>
					<select name="propertyMinPrice" class="form-control" #propertyMinPrice>
					  <option value="Any">Any</option>
					  <option value="1">50,000</option>
					  <option value="2">100,000</option>
					  <option value="3">200,000</option>
					  <option value="4">300,000</option>
					  <option value="5">400,000</option>
					  <option value="6">500,000</option>
					  <option value="7">600,000</option>
					</select>
				</div>
				<div  class="col-md-2 form-control">
					<label for="title">Max. Price:</label>
					<select name="propertyMaxPrice" class="form-control" #propertyMaxPrice>
					  <option value="Any">Any</option>
					  <option value="1">50,000</option>
					  <option value="2">100,000</option>
					  <option value="3">200,000</option>
					  <option value="4">300,000</option>
					  <option value="5">400,000</option>
					  <option value="6">500,000</option>
					  <option value="7">600,000</option>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<div  class="col-md-3 form-control">
					<label for="title">State:</label>
					<select name="state" class="form-control" #state>
					  <option value="Any">Any</option>
					  <option value="1">New South Wales</option>
					  <option value="2">ACT</option>
					  <option value="3">Victoria</option>
					  <option value="4">South Australia</option>
					  <option value="5">West Australia</option>
					  <option value="6">North Territory</option>
					  <option value="7">Quensland</option>
					</select>
				</div>
				<div  class="col-md-3 form-control">
					<label for="ares">City:</label>
					<input name="city" #city class="form-control">
				</div>	
				<div  class="col-md-3 form-control">
					<label for="ares">Area:</label>
					<ng2-completer type=input [(ngModel)]="myState" [ngModelOptions]="{standalone: true}" [datasource]="myStates" [minSearchLength]="0" class="form-control" name="area" #area></ng2-completer>
				</div>

				<div  class="col-md-3 form-control">
					<label for="ares">Key words:</label>
					<input name="word" #word  class="form-control">
				</div>
			</div>
			<div class="form-group row">
	      		<div class="">
					<button class="btn btn-primary" (click)="addArticle(propertyType, minBeds,maxBeds,area)">
						Search
					</button>
				</div>
			</div>

		</form>
	</div>
		

	<div class="">
		<property-search-result *ngFor="let result of results" [result]="result">
		</property-search-result>	
	</div>
  `
})

class PropertySelector{
	protected myState: string;
	protected myStates = ['New South Wales', 'Australian Capital Territory', 'Queensland', 'South Australia', 'Western Australia', 'Tasmania'];
	results:Result[];
	constructor(){
		this.results=[];
	}
	addArticle(propertyType: HTMLInputElement, minBeds: HTMLInputElement,maxBeds: HTMLInputElement, area: HTMLInputElement): boolean {
		var searchResults=[
		{"propertyType":"house", "beds":"3", "suburb":"Cherrybrook", "address":"56 County Dr", "State_short":"NSW", "State_long":"New South Wales", "Country_long":"Australia", "Country_short":"AUS", "coords":[-33.728846, 151.032389],"Title":"AUCTION 24th JUNE AT 11:30AM","Agent":"Igor Guvic","Details":"Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.","images":["1","2","3","4","5","6"]},

		{"propertyType":"house","beds":"4","suburb":"Cherrybrook","address":"6 Dantic Pl","State_short":"NSW","State_long":"New South Wales","Country_long":"Australia","Country_short":"AUS","coords":[-33.727493, 151.030418],"Title":"AUCTION 24th JUNE AT 11:30AM","Agent":"Igor Guvic","Details":"Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.","images":["1","2","3","4","5","6"]},

		{"propertyType":"app_unit","beds":"4","suburb":"Cherrybrook","address":"12A Haven Ct", "State_short":"NSW","State_long":"New South Wales", "Country_long":"Australia","Country_short":"AUS","coords":[-33.728526, 151.029384],"Title":"AUCTION 24th JUNE AT 11:30AM", "Agent":"Igor Guvic","Details":"Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.","images":["1","2","3","4","5","6"]},

		{"propertyType":"app_unit","beds":"4","suburb":"Cherrybrook","address":"47 County Dr","State_short":"NSW","State_long":"New South Wales","Country_long":"Australia","Country_short":"AUS","coords":[-33.729630, 151.030639],"Title":"AUCTION 24th JUNE AT 11:30AM","Agent":"Igor Guvic","Details":"Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.","images":["1","2","3","4","5","6"]},

		{"propertyType":"app_unit","beds":"5","suburb":"Cherrybrook","address":"66 John Rd", "State_short":"NSW","State_long":"New South Wales","Country_long":"Australia","Country_short":"AUS","coords":[-33.730674, 151.034115],"Title":"AUCTION 24th JUNE AT 11:30AM","Agent":"Igor Guvic","Details":"Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.","images":["1","2","3","4","5","6"]},

		{"propertyType":"house","beds":"5","suburb":"Cherrybrook","address":"153 David Rd", "State_short":"NSW","State_long":"New South Wales","Country_long":"Australia","Country_short":"AUS","coords":[-33.727248, 151.029062],"Title":"AUCTION 24th JUNE AT 11:30AM","Agent":"Igor Guvic","Details":"Situated in a sought-after area of Baulkham Hills, this immaculate and tastefully presented home is going to be a family favourite. Centrally positioned, only moments away from Norwest Business Park, the future train station, Norwest Marketown, Stockland Mall, Winston Hills Mall, Castle Towers Shopping Centre, the local restaurant precinct, M2/M7 and the local bus network including the city buses.","images":["1","2","3","4","5","6"]}
		];


		for (var searchResult of searchResults) {
			if((searchResult.propertyType==propertyType.value ||propertyType.value=='All') && ((searchResult.beds>=minBeds.value && searchResult.beds<=maxBeds.value))){
				this.results.push(searchResult));
			}
		}
		return false;
	}
}

/*Main class*/
@Component({
	selector: 'property-app',
	template: `
		<div class="property-app">
			<property-search></property-search>
	    </div>
`})

class PropertyApp{}

@NgModule({
  declarations: [PropertyApp,PropertySelector,SearchResult],
  imports: [ BrowserModule,Ng2CompleterModule,FormsModule ],
  bootstrap: [PropertyApp]
})
class SearchProperies {}

platformBrowserDynamic().bootstrapModule(SearchProperies);