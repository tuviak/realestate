﻿
using AllHomes.Models;
using System;
using System.Linq;

namespace AllHomes.Data
{
    public static class DbInitializer
    {
        public static void Initialize(AllHomesContext context)
        {
            context.Database.EnsureCreated();

            // Look for any States.
            if (context.Locations.Any())
            {
                return;   // DB has been seeded
            }

            var locations = new Location[]
            {
            new Location{Title="Very Good House",HouseNumber="1",AdvDate=DateTime.Parse("2005-09-01")},
            new Location{Title="Very Good House",HouseNumber="2",AdvDate=DateTime.Parse("2002-09-01")},
            new Location{Title="Very Good House",HouseNumber="3",AdvDate=DateTime.Parse("2003-09-01")},
            new Location{Title="Very Good House",HouseNumber="4",AdvDate=DateTime.Parse("2002-09-01")},
            new Location{Title="Very Good House",HouseNumber="5",AdvDate=DateTime.Parse("2002-09-01")},
            new Location{Title="Very Good House Peggy",HouseNumber="6",AdvDate=DateTime.Parse("2001-09-01")},
            new Location{Title="Very Good House Laura",HouseNumber="7",AdvDate=DateTime.Parse("2003-09-01")},
            new Location{Title="Very Good House Nino",HouseNumber="8",AdvDate=DateTime.Parse("2005-09-01")}
            };
            foreach (Location l in locations)
            {
                context.Locations.Add(l);
            }
            context.SaveChanges();

            var cities = new City[]
            {
            new City{CityID=1050,CityName="Cherrybrook"},
            new City{CityID=4022,CityName="Cherrybrook"},
            new City{CityID=4041,CityName="Cherrybrook"},
            new City{CityID=1045,CityName="Castle Hill"},
            new City{CityID=3141,CityName="Castle Hill"},
            new City{CityID=2021,CityName="Castle Hill"},
            new City{CityID=2042,CityName="Castle Hill"}
            };
            foreach (City c in cities)
            {
                context.Cities.Add(c);
            }
            context.SaveChanges();

            var states = new State[]
            {
            new State{StateID=1,StateName="New South Wales"},
            new State{StateID=2,StateName="Victoria"},
            new State{StateID=3,StateName="South Australia"},
            new State{StateID=4,StateName="Western Australia"},
            new State{StateID=5,StateName="North Territory"},
            new State{StateID=6,StateName="Capital Territory"},
            };
            foreach (State s in states)
            {
                context.States.Add(s);
            }
            context.SaveChanges();
        }
    }
}