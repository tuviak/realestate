﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AllHomes.Models
{
    public class Location
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string HouseNumber { get; set; }
        public string Street { get; set; }
        public string Postcode { get; set; }
        public decimal Price { get; set; }
        public int Beds { get; set; }
        public int Parkings { get; set; }
        public int PropertyType { get; set; }
        public DateTime AdvDate { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public City City { get; set; }
        public State State { get; set; }
    }
}
