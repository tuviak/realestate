﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AllHomes.Models
{
    public class AllHomesContext : DbContext
    {
        public AllHomesContext (DbContextOptions<AllHomesContext> options)
            : base(options)
        {
        }

        public DbSet<Location> Locations { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<State> States { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().ToTable("City");
            modelBuilder.Entity<Location>().ToTable("Location");
            modelBuilder.Entity<State>().ToTable("State");
        }
    }

    
}
