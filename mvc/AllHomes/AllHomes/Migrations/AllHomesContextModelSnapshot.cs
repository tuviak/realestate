﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using AllHomes.Models;

namespace AllHomes.Migrations
{
    [DbContext(typeof(AllHomesContext))]
    partial class AllHomesContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("AllHomes.Models.locations", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AdvDate");

                    b.Property<int>("Beds");

                    b.Property<int>("City");

                    b.Property<string>("HouseNumber");

                    b.Property<decimal>("Latitude");

                    b.Property<decimal>("Longitude");

                    b.Property<int>("Parkings");

                    b.Property<string>("Postcode");

                    b.Property<decimal>("Price");

                    b.Property<int>("PropertyType");

                    b.Property<int>("State");

                    b.Property<string>("Street");

                    b.Property<string>("Title");

                    b.HasKey("ID");

                    b.ToTable("locations");
                });
        }
    }
}
